//
//  UIView+Shadow.h
//  MuscleCar
//
//  Created by Sergey Borovikov on 24/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Shadow)

- (UIView *)addShadow;

@end
