//
//  AppDelegate.h
//  MuscleCar
//
//  Created by Sergey Borovikov on 23/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

