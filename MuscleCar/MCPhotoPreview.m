//
//  MCPhotoPreview.m
//  MuscleCar
//
//  Created by Sergey Borovikov on 21/12/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import "MCPhotoPreview.h"

@interface MCPhotoPreview ()
@property (weak, nonatomic) IBOutlet UIImageView *photoPreviewIV;
@end

@implementation MCPhotoPreview

- (void)viewWillAppear:(BOOL)animated {
    _photoPreviewIV.image = _photo.image;
}

- (IBAction)tapBtnTouched:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
