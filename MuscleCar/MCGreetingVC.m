//
//  GreetingViewController.m
//  MuscleCar
//
//  Created by Sergey Borovikov on 23/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import "MCPhotoManager.h"
#import "MCGreetingVC.h"
#import "UIViewController+Colors.h"
#import "UIView+Shadow.h"
#import "MCInfoVC.h"

@interface MCGreetingVC () <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;

// Nav Bar View
@property (weak, nonatomic) IBOutlet UIView *navBarView;
@property (weak, nonatomic) IBOutlet UIView *infoBtnView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMuscleCarLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthMuscleCarLogoIV;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sInfoBtnView;

// Labels
@property (weak, nonatomic) IBOutlet UILabel *firstGreetingLB;
@property (weak, nonatomic) IBOutlet UILabel *secondGreetingLB;
@property (weak, nonatomic) IBOutlet UILabel *fiveLB;
@property (weak, nonatomic) IBOutlet UILabel *minutesLB;

// Begin Btn View
@property (weak, nonatomic) IBOutlet UIView *beginBtnView;
@property (weak, nonatomic) IBOutlet UILabel *beginBtnLB;

@end

@implementation MCGreetingVC {
    MCPhotoManager *photoManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    photoManager = [MCPhotoManager shr];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self p_setColorForViews];
    [self p_setFontAndColorForLabels];
    [self p_prepearViewForStartAnimations];
}

- (void)viewWillAppear:(BOOL)animated {
    
    _beginBtnView.layer.cornerRadius = 2;
    [_beginBtnView addShadow];
}

- (void)viewDidAppear:(BOOL)animated {
    
}

- (IBAction)beginBtnTouched:(UIButton *)sender {
    [self performSegueWithIdentifier:@"toMainVC" sender:nil];
}

- (void)p_setColorForViews {
    
    self.view.backgroundColor     = [self defaultBGColor];
    _navBarView.backgroundColor   = [self defaultBGColor];
    _beginBtnView.backgroundColor = [self defaultBtnColor];
    _beginBtnLB.textColor         = [self defaultBGColor];
}

- (void)p_setFontAndColorForLabels {
    
    _firstGreetingLB.font  = [UIFont fontWithName: @"RoadRadio-Light" size: 20.0];
    _secondGreetingLB.font = [UIFont fontWithName: @"RoadRadio-Thin"  size: 16.0];
    _fiveLB.font           = [UIFont fontWithName: @"RoadRadio-Light" size: 40.0];
    _minutesLB.font        = [UIFont fontWithName: @"RoadRadio-Light" size: 14.0];
    
    _firstGreetingLB.textColor  = [self defaultTextColor];
    _secondGreetingLB.textColor = [self defaultTextColor];
    _fiveLB.textColor           = [self defaultRedColor];
    _minutesLB.textColor        = [self defaultRedColor];
}

- (void)p_checkUserData {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"damagePhotosCount"] integerValue] > 0 ||
        [[[NSUserDefaults standardUserDefaults] objectForKey:@"carPhotosCount"] integerValue]    > 0 ||
        
        (![[[NSUserDefaults standardUserDefaults] objectForKey:@"damageDitail"] isEqualToString:@""] &&
        [[NSUserDefaults standardUserDefaults] objectForKey:@"damageDitail"] != nil)) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Есть неотправленные данные"
                                                        message: @"Начать сначала или продолжить?"
                                                       delegate: self
                                              cancelButtonTitle: @"Сначала"
                                              otherButtonTitles: @"Продолжить", nil];
        alert.tag = 1;
        [alert show];
    }
}

# pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 1) {
        if (buttonIndex == 0) {
            
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"damageDitail"];
            [photoManager removeAllPhotos];
            
        } else if (buttonIndex == 1) {
            [self performSegueWithIdentifier:@"toMainVC" sender:nil];
        }
    }
}

# pragma mark - fanctions for animations
- (void)p_prepearViewForStartAnimations {
    
    _contentView.alpha = 0;
    _topMuscleCarLogo.constant = 240;
    _infoBtnView.alpha = 0;
    _sInfoBtnView.constant = -50;
    _widthMuscleCarLogoIV.constant = 260;
    
    [NSTimer scheduledTimerWithTimeInterval: 1
                                     target: self
                                   selector: @selector(p_startLogoAnimation)
                                   userInfo: nil
                                    repeats: NO];
}

- (void)p_startLogoAnimation {
    [UIView animateWithDuration:0.8 animations:^{
        _topMuscleCarLogo.constant = 16;
        _widthMuscleCarLogoIV.constant = 180;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self p_beginStartAnimations];
    }];
}

- (void)p_beginStartAnimations {
    [UIView animateWithDuration: 0.3 animations:^{
        _contentView.alpha = 1;
        _infoBtnView.alpha = 1;
        _sInfoBtnView.constant = 0;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self p_checkUserData];
    }];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue destinationViewController] isKindOfClass:[MCInfoVC class]]) {
        MCInfoVC *infoVC = (MCInfoVC *)[segue destinationViewController];
        infoVC.presentedVCIndex = @0;
    }
}

@end
