//
//  MCNavigationController.m
//  MuscleCar
//
//  Created by Sergey Borovikov on 16/12/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import "MCNavigationController.h"

@implementation MCNavigationController

-(BOOL)shouldAutorotate {
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}

@end
