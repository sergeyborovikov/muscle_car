//
//  UIViewController+Colors.h
//  MuscleCar
//
//  Created by Sergey Borovikov on 23/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+BFPaperColors.h"

@interface UIViewController (Colors)

-(UIColor*)defaultBGColor;
-(UIColor*)defaultDarkBGColor;
-(UIColor*)defaultTextColor;
-(UIColor*)defaultBtnColor;
-(UIColor*)defaultRedColor;

-(UIColor*)defaultBGColorWithAlpha;

@end
