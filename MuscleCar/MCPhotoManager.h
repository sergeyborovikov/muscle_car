//
//  PhotoManager.h
//  MuscleCar
//
//  Created by Sergey Borovikov on 27/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCPhotoMaker.h"

@interface MCPhotoManager : NSObject

+ (instancetype)shr;

- (void)savePhotos:(NSArray *)photos photoType:(MCPhotoType)photoType;
- (void)saveOnePhoto:(NSData *)photoData photoType:(MCPhotoType)photoType;
- (NSArray *)getAllPhotosFromDocuments;
- (NSArray *)getDamagePhotosFromDocuments;
- (NSArray *)getCarPhotosFromDocuments;
- (void)removeAllPhotos;
- (void)removeDamagePhotos;
- (void)removeCarPhoto;
- (BOOL)areThereDamagePhotoInDocuments;
- (BOOL)areThereCarPhotoInDocuments;

@end
