//
//  UIView+Shadow.m
//  MuscleCar
//
//  Created by Sergey Borovikov on 24/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import "UIView+Shadow.h"

@implementation UIView (Shadow)

- (UIView *)addShadow {
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:[self bounds]];
    self.layer.shadowColor   = [UIColor blackColor].CGColor;
    self.layer.shadowOffset  = CGSizeMake(0, 2);
    self.layer.shadowOpacity = 0.4;
    self.layer.shadowRadius  = 3;
    self.layer.shadowPath    = shadowPath.CGPath;
    return self;
}

@end
