//
//  MCPhotoPreview.h
//  MuscleCar
//
//  Created by Sergey Borovikov on 21/12/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MCPhotoPreview : UIViewController

@property UIImageView *photo;

@end
