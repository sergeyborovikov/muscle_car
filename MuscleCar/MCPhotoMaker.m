//
//  PhotoMaker.m
//  DTP
//
//  Created by Sergey on 02/09/14.
//  Copyright (c) 2014 Unlim. All rights reserved.
//

#import "MCPhotoManager.h"
#import "MCPhotoMaker.h"
#import "MCPhotoVC.h"
#import "MCPhotoPreview.h"
#import "UIViewController+Colors.h"
#import <AVFoundation/AVFoundation.h>
#import <ImageIO/ImageIO.h>

@interface MCPhotoMaker ()

@property (weak, nonatomic) IBOutlet UIView   *vImagePreview;
@property (weak, nonatomic) IBOutlet UILabel  *numPhotoLb;
@property (weak, nonatomic) IBOutlet UIImageView *cameraIV;
@property (weak, nonatomic) IBOutlet UIImageView *cameraSideIV;
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;
@property (weak, nonatomic) IBOutlet UIButton *doneSideBtn;
@property (weak, nonatomic) IBOutlet UIView   *makeBtnView;
@property (weak, nonatomic) IBOutlet UIView   *makeSideBtnView;
@property (weak, nonatomic) IBOutlet UIView   *topView;
@property (weak, nonatomic) IBOutlet UIView   *bottomView;
@property (weak, nonatomic) IBOutlet UIView   *sideView;
@property (weak, nonatomic) IBOutlet UIImageView *backBtnIV;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bPicks;

// photos
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *photoIVs;

@property (nonnull) NSMutableArray *photosData;

@end

@implementation MCPhotoMaker {
    AVCaptureSession          *session;
    AVCaptureStillImageOutput *stillImageOutput;
    MCPhotoVC *photoVC;
    CGRect tmpPicRect;
    NSUInteger currentPhotoIndex;
    UITapGestureRecognizer *tap;
    BOOL miniPhotoIsOpen;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self p_checkSavedPhotos];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(p_didRotate)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    
    [self p_setColorsForViews];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [self p_setUpCaptureSession];
    
    for (UIImageView *photo in _photoIVs) {
        [photo.layer setMasksToBounds:YES];
    }
    
    _presentationParent.fromCamera = YES;
}

- (void)p_checkSavedPhotos {
    
    if (_photoType == MCPhotoTypeDamage) {
        _photosData = [[[MCPhotoManager shr] getDamagePhotosFromDocuments] mutableCopy];
    } else if (_photoType == MCPhotoTypeCar) {
        _photosData = [[[MCPhotoManager shr] getCarPhotosFromDocuments] mutableCopy];
    }
}
                   
- (void)p_setUpCaptureSession {
    
    [self p_markUpPhotos];
    
    if (session) {
        [session stopRunning];
        session = nil;
        stillImageOutput = nil;
    }
    
    session = [[AVCaptureSession alloc] init];
    session.sessionPreset = AVCaptureSessionPreset1920x1080;
    
    AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    if (self.vImagePreview.layer.sublayers.count > 1) {
        [self.vImagePreview.layer.sublayers[0] removeFromSuperlayer];
    }
    [self.vImagePreview.layer addSublayer:captureVideoPreviewLayer];
    
    NSLog(@"%@", self.vImagePreview.layer.sublayers);
    
    CGFloat _height = [[UIScreen mainScreen] bounds].size.height;
    CGFloat _width  = [[UIScreen mainScreen] bounds].size.width;
    
    captureVideoPreviewLayer.frame = CGRectMake(0, 0, _width, _height);
    
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType: AVMediaTypeVideo];
    
    NSError *error = nil;
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    if (!input) {
        // Handle the error appropriately.
        NSLog(@"ERROR: trying to open camera: %@", error);
    }
    
    [session addInput:input];
    if (captureVideoPreviewLayer.connection.supportsVideoOrientation) {
        captureVideoPreviewLayer.connection.videoOrientation = [self deviceOrientationToVideoOrientation: orientation];
    }
    stillImageOutput = [AVCaptureStillImageOutput new];
    [stillImageOutput setOutputSettings:@{AVVideoCodecKey:AVVideoCodecJPEG}];
    [session addOutput:stillImageOutput];
    [session startRunning];
}

- (AVCaptureVideoOrientation)deviceOrientationToVideoOrientation:(UIDeviceOrientation)orientation {
    switch (orientation) {
        case UIInterfaceOrientationPortrait:
            return AVCaptureVideoOrientationPortrait;
        case UIInterfaceOrientationLandscapeLeft:
            return AVCaptureVideoOrientationLandscapeLeft;
        case UIInterfaceOrientationLandscapeRight:
            return AVCaptureVideoOrientationLandscapeRight;
        default:
            break;
    }
    NSLog(@"Warning - Didn't recognise interface orientation (%ld)",(long)orientation);
    return AVCaptureVideoOrientationPortrait;
}

- (void)p_setColorsForViews {
    _backBtnIV.image = [_backBtnIV.image imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
    _backBtnIV.tintColor = [UIColor whiteColor];
    _topView.backgroundColor    = [self defaultBGColorWithAlpha];
    _bottomView.backgroundColor = [self defaultBGColorWithAlpha];
    _sideView.backgroundColor   = [self defaultBGColorWithAlpha];
    _topView.alpha    = 1;
    _sideView.alpha   = 0;
    _bottomView.alpha = 1;
    _cameraIV.image = [_cameraIV.image imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
    _cameraSideIV.image = [_cameraSideIV.image imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
    _cameraIV.tintColor = [UIColor whiteColor];
    _cameraSideIV.tintColor = [UIColor whiteColor];
    _makeBtnView.backgroundColor = [self defaultDarkBGColor];
    _makeSideBtnView.backgroundColor = [self defaultDarkBGColor];
    _makeBtnView.layer.cornerRadius  = _makeBtnView.frame.size.width/2;
    _makeSideBtnView.layer.cornerRadius = _makeSideBtnView.frame.size.width/2;
    [_doneBtn     setTitleColor: [UIColor whiteColor] forState: UIControlStateNormal];
    [_doneSideBtn setTitleColor: [UIColor whiteColor] forState: UIControlStateNormal];
}

- (void)p_didRotate {
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    if (orientation == UIDeviceOrientationPortrait) {
        [self p_showBottomView];
    } else if (orientation == UIDeviceOrientationLandscapeLeft ||
               orientation == UIDeviceOrientationLandscapeRight){
        [self p_showSideView];
    }
    
    if (orientation != UIDeviceOrientationPortraitUpsideDown &&
        orientation != UIDeviceOrientationFaceUp &&
        orientation != UIDeviceOrientationFaceDown) {
        [self p_setUpCaptureSession];
    }
}

- (void)p_showSideView {
    [UIView animateWithDuration: 0.3 animations: ^{
        self.topView.backgroundColor = [UIColor clearColor];
        self.sideView.alpha   = 1;
        self.bottomView.alpha = 0;
        self.bPicks.constant = - 60;
        [self.view layoutIfNeeded];
    }];
}

- (void)p_showBottomView {
    [UIView animateWithDuration: 0.3 animations: ^{
        self.topView.backgroundColor = [self defaultBGColorWithAlpha];
        self.sideView.alpha   = 0;
        self.bottomView.alpha = 1;
        self.bPicks.constant  = 8;
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)makePhoto {
    
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in stillImageOutput.connections) {
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] ) {
                videoConnection = connection;
                videoConnection.videoOrientation = [self deviceOrientationToVideoOrientation:[[UIDevice currentDevice] orientation]];
                break;
            }
        }
        if (videoConnection) { break; }
    }
    
    NSLog(@"about to request a capture from: %@", stillImageOutput);
    
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection
                                                  completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
                                                      
                                                      NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
                                                      
                                                      [self p_addNewPhoto:imageData];
                                                  }];
}

- (void)p_addNewPhoto:(NSData *)newPhotoData {
    
    NSData *preparedPhotoData = [self p_preparPhotoForSaving:newPhotoData];
    
    if (_photoType == MCPhotoTypeDamage) {
        
        if (_photosData.count < 3) {
            [_photosData addObject:preparedPhotoData];
        } else {
            [_photosData removeObjectAtIndex:0];
            [_photosData addObject:preparedPhotoData];
        }
    } else if (_photoType == MCPhotoTypeCar) {
        
        if (_photosData.count == 0) {
            [_photosData addObject:preparedPhotoData];
        } else {
            [_photosData removeObjectAtIndex:0];
            [_photosData addObject:preparedPhotoData];
        }
    }
    
    [self p_markUpPhotos];
}

- (NSData *)p_preparPhotoForSaving:(NSData *)photoData {
    
    UIImage *newPhotoI = [UIImage imageWithData:photoData];
    NSData *result;
    result = UIImageJPEGRepresentation(newPhotoI, 0.5);
    
    return result;
}

- (void)p_markUpPhotos {
    NSString *maxPhotoCount = _photoType == MCPhotoTypeCar ? @"1" : @"3";
    _numPhotoLb.text = [NSString stringWithFormat:@"Фотография %li из %@", (long)[_photosData count], maxPhotoCount];
    
    for (NSInteger i = 0; i < _photosData.count; i++) {
        UIImage *photo = [UIImage imageWithData: _photosData[i]];
        UIImageView *photoIV = _photoIVs[i];
        photoIV.image = photo;
    }
}

- (void)p_showPics {
    for (UIImageView *photoIV in _photoIVs) { photoIV.alpha = 1; }
}

- (void)p_hidePics {
    for (UIImageView *photoIV in _photoIVs) { photoIV.alpha = 0; }
}

- (IBAction)doneBtnTouched {
    
    if([[UIDevice currentDevice] orientation] != UIDeviceOrientationPortrait) {
        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
        [[UIDevice currentDevice] setValue: value forKey: @"orientation"];
    }
    
    if (_photosData.count > 0) {
        [_delegate savePhotos: _photosData photoType: _photoType];
        [self dismissViewControllerAnimated:YES completion:^{
            [self.presentationParent checkChosenPhotoCount: nil];
        }];
    } else {
        
        [self dismissViewControllerAnimated: YES completion: nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton *)sender {
    if ([segue.destinationViewController isKindOfClass:[MCPhotoPreview class]]) {
        MCPhotoPreview *photoPreview = [segue destinationViewController];
        photoPreview.photo = _photoIVs[sender.tag];
    }
}

@end



















