//
//  MainVC.h
//  MuscleCar
//
//  Created by Sergey Borovikov on 23/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCPhotoMaker.h"

@interface MCMainVC : UIViewController <MCPhotoMakerDelegate>

- (void)markUpPhotoIcons;

@end
