//
//  UIViewController+InputAccessoryView.h
//  MuscleCar
//
//  Created by Sergey Borovikov on 24/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MCInputAccesViewDelegate <NSObject>

- (void)okTouched;
- (void)cancelTouched;

@end

@interface UIViewController (InputAccessoryView)

- (UIView *)inputAccesView;

@end
