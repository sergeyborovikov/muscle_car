//
//  MCNavigationController.h
//  MuscleCar
//
//  Created by Sergey Borovikov on 16/12/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MCNavigationController : UINavigationController

@end
