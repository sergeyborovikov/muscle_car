//
//  InfoVC.m
//  MuscleCar
//
//  Created by Sergey Borovikov on 23/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import "MCInfoVC.h"
#import "UIViewController+Colors.h"
#import "UIViewController+PhoneFormat.h"
#import "UIViewController+InputAccessoryView.h"
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>

#import <MailCore/MailCore.h>

@interface MCInfoVC ()
<UITextFieldDelegate,
MCInputAccesViewDelegate,
CNContactViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *titleLB;

// Nav Bar View
@property (weak, nonatomic) IBOutlet UIView      *navBarView;
@property (weak, nonatomic) IBOutlet UIImageView *muscleCarLogoIV;
@property (weak, nonatomic) IBOutlet UIView      *backBtnView;
@property (weak, nonatomic) IBOutlet UIImageView *arrowLeftIV;

// BTN views
@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UIView *phoneView;
@property (weak, nonatomic) IBOutlet UIView *webSideView;
@property (weak, nonatomic) IBOutlet UIView *saveContactView;
@property (weak, nonatomic) IBOutlet UIView *callMeView;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UILabel *plusSevenLB;

@property (weak, nonatomic) IBOutlet UILabel *addressLB;
@property (weak, nonatomic) IBOutlet UILabel *phoneLB;
@property (weak, nonatomic) IBOutlet UILabel *webSiteLB;
@property (weak, nonatomic) IBOutlet UILabel *saveToContactsLB;
@property (weak, nonatomic) IBOutlet UILabel *callMeLB;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activtyIndicator;

@end

@implementation MCInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _phoneTF.inputAccessoryView = [self inputAccesView];
    
    [self p_setColorForLables];
    [self p_setColorForViews];
}

- (void)viewWillAppear:(BOOL)animated {
    [self p_setPhoneNumberTF];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewDidDisappear:(BOOL)animated {
    [self p_savePhoneNumber];
}

- (void)p_setColorForViews {
    
    self.view.backgroundColor    = [self defaultBGColor];
    _titleLB.textColor           = [self defaultRedColor];
    _navBarView.backgroundColor  = [self defaultBGColor];
    _addressView.backgroundColor = [self defaultDarkBGColor];
    _phoneView.backgroundColor   = [self defaultDarkBGColor];
    _webSideView.backgroundColor = [self defaultDarkBGColor];
    _saveContactView.backgroundColor = [self defaultDarkBGColor];
    _lineView.backgroundColor    = [self defaultRedColor];
    _callMeView.backgroundColor  = [self defaultDarkBGColor];
    
    _lineView.backgroundColor = [self defaultRedColor];
    _activtyIndicator.color = [self defaultTextColor];
}

- (void)p_setColorForLables {
    
    _addressLB.textColor        = [self defaultTextColor];
    _phoneLB.textColor          = [self defaultTextColor];
    _webSiteLB.textColor        = [self defaultTextColor];
    _saveToContactsLB.textColor = [self defaultTextColor];
    _callMeLB.textColor         = [self defaultTextColor];
    _plusSevenLB.textColor      = [self defaultTextColor];
    _phoneTF.textColor          = [self defaultTextColor];
    
    // set custom color for placeholder
    [_phoneTF setValue:[self defaultTextColor] forKeyPath:@"_placeholderLabel.textColor"];
}

- (void)p_savePhoneNumber {
    [[NSUserDefaults standardUserDefaults] setObject: _phoneTF.text forKey:@"myPhone"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)p_setPhoneNumberTF {
    NSString *phoneNumberStr = [[NSUserDefaults standardUserDefaults] objectForKey: @"myPhone"];
    _phoneTF.text = phoneNumberStr == nil ? @"" : phoneNumberStr;
}

# pragma mark UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGFloat newOffsetY = textField.frame.origin.y - 80;
    [_scrollView setContentOffset:CGPointMake(0.0, newOffsetY) animated:YES];
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [_scrollView setContentOffset: CGPointMake(0,0) animated: YES];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    
    if (range.length == 1) {
        // Delete button was hit..
        textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
    } else {
        textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
    }
    return NO;
}

# pragma mark InputAccesViewDelegate
- (void)okTouched     { [self.view endEditing: YES]; }
- (void)cancelTouched { [self.view endEditing: YES]; }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [self p_savePhoneNumber];
}

- (IBAction)backBtn:(UIButton *)sender {
    [self dismissViewControllerAnimated: YES completion: nil];
    
    [self.navigationController popToViewController: self.navigationController.viewControllers[[_presentedVCIndex integerValue]]
                                          animated: YES];
}

- (IBAction)openMuscleCarSite:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.musclecar66.ru"]];
}

- (IBAction)call:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://83433288861"]];
}

- (IBAction)saveContactBtnTouched:(UIButton *)sender {

    CNContactStore *store = [[CNContactStore alloc] init];
    
    // create contact
    CNMutableContact *contact = [[CNMutableContact alloc] init];
    contact.familyName = @"Автоцентр";
    contact.givenName  = @"Мускулкар";
    
    CNLabeledValue *homePhone = [CNLabeledValue labeledValueWithLabel:CNLabelHome value:[CNPhoneNumber phoneNumberWithStringValue:@"+7 (343) 328-88-61"]];
    CNLabeledValue *email = [CNLabeledValue labeledValueWithLabel:CNLabelEmailiCloud value:@"pdr@musclecar66.ru"];
    CNLabeledValue *url = [CNLabeledValue labeledValueWithLabel:CNLabelURLAddressHomePage value:@"http://www.musclecar66.ru"];
    contact.phoneNumbers = @[homePhone];
    contact.emailAddresses = @[email];
    contact.urlAddresses = @[url];
    
    CNContactViewController *contactController = [CNContactViewController viewControllerForUnknownContact:contact];
    contactController.contactStore = store;
    contactController.delegate = self;
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController pushViewController:contactController animated:YES];
}

- (IBAction)recallBtnTouched:(id)sender {
    
    if(_activtyIndicator.isAnimating) { return; }
    
    if (_phoneTF.text.length < 14 ) {
        [self p_showWrongPhoneNumber];
        return;
    }
    
    [_activtyIndicator startAnimating];
    
    MCOSMTPSession *smtpSession = [[MCOSMTPSession alloc] init];
    smtpSession.hostname = @"smtp.mail.ru";
    smtpSession.port     = 465;
    smtpSession.username = @"musclecar-mail@mail.ru";
    smtpSession.password = @"s+WZkH*Sna3sXL";
    smtpSession.authType = MCOAuthTypeSASLNone;
    smtpSession.connectionType = MCOConnectionTypeTLS;
    
    MCOMessageBuilder *builder = [[MCOMessageBuilder alloc] init];
    MCOAddress *from = [MCOAddress addressWithDisplayName:@"musclecar-mail@mail.ru" mailbox:@"musclecar-mail@mail.ru"];
    MCOAddress *to      = [MCOAddress addressWithDisplayName: nil mailbox: @"pdr@musclecar66.ru"];
    MCOAddress *toTest1 = [MCOAddress addressWithDisplayName: nil mailbox: @"sergey.borovikov@list.ru"];
    MCOAddress *toTest2 = [MCOAddress addressWithDisplayName: nil mailbox: @"ilya.nikulin@gmail.com"];
    
    NSDateFormatter *dateFormator = [[NSDateFormatter alloc] init];
    [dateFormator setDateFormat:@"dd MMM YYYY  hh:mm"];
    [dateFormator setTimeZone: [NSTimeZone localTimeZone]];
    NSString *date = [dateFormator stringFromDate: [NSDate date]];
    NSString *subject = [NSString stringWithFormat: @"iOS Muscle Car тел: +7 %@", _phoneTF.text];
    NSString *body = [NSString stringWithFormat: @"Дата: %@ \n Контакт: +7 %@ \n Перезвоните мне", date, _phoneTF.text ];
    
    [[builder header] setFrom: from];
    [[builder header] setTo: @[to, toTest1, toTest2]];
    [[builder header] setSubject: subject];
    [builder setTextBody: body];
    NSData *msgData = [builder data];
    
    MCOSMTPSendOperation *sendOpertion = [smtpSession sendOperationWithData: msgData];
    [sendOpertion start: ^(NSError *error) {
        if(error) {
            [self p_showFaildSendEmailAlert];
        } else {
            [self p_showSentEmailAlert];
        }
        [_activtyIndicator stopAnimating];
    }];
}


-(void)p_showSentEmailAlert {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Запрос на обратный звонок отправлен"
                                                    message: nil
                                                   delegate: nil
                                          cancelButtonTitle: @"Закрыть"
                                          otherButtonTitles: nil];
    [alert show];
}

- (void)p_showFaildSendEmailAlert {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Невозможно отправить данные"
                                                    message: @"Почтовый сервис недоступен. Пожалуйста, попробуйте отправить данные позже"
                                                   delegate: self
                                          cancelButtonTitle: @"Закрыть"
                                          otherButtonTitles: @"Отправить", nil];
    alert.tag = 2;
    [alert show];
}

- (void)p_showWrongPhoneNumber {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Некоректный номер телефона"
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle: @"Продолжить"
                                          otherButtonTitles: nil];
    [alert show];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
