//
//  PhotoMaker.h
//  DTP
//
//  Created by Sergey on 02/09/14.
//  Copyright (c) 2014 Unlim. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MCPhotoVC;

typedef NS_ENUM(NSUInteger, MCPhotoType) {
    MCPhotoTypeDamage,
    MCPhotoTypeCar,
};

@protocol MCPhotoMakerDelegate <NSObject>

- (void)savePhotos:(NSArray *)photos photoType:(MCPhotoType)photoType;

@end

@interface MCPhotoMaker : UIViewController

@property (weak, nonatomic) id <MCPhotoMakerDelegate> delegate;
@property (strong) MCPhotoVC *presentationParent;
@property MCPhotoType photoType;

@end
