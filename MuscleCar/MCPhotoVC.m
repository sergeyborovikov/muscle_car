//
//  PhotoVC.m
//  MuscleCar
//
//  Created by Sergey Borovikov on 25/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import "MCMainVC.h"
#import "MCPhotoVC.h"
#import "MCPhotoMaker.h"
#import "MCPhotoManager.h"
#import "UIViewController+Colors.h"
#import "UIImage+Resize.h"

@interface MCPhotoVC () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIView  *contentView;
@property (weak, nonatomic) IBOutlet UILabel *damagePhotoLB;

// BTNs views
@property (weak, nonatomic) IBOutlet UIView  *makePhotoView;
@property (weak, nonatomic) IBOutlet UILabel *makePhotoLB;
@property (weak, nonatomic) IBOutlet UIImageView *makePhotoIV;
@property (weak, nonatomic) IBOutlet UIView  *pickPhotoView;
@property (weak, nonatomic) IBOutlet UIImageView *pickPhotoIV;
@property (weak, nonatomic) IBOutlet UILabel *pickPhotoLB;

@property (weak, nonatomic) IBOutlet UILabel *firstLB;
@property (weak, nonatomic) IBOutlet UILabel *secondLB;

@end

@implementation MCPhotoVC

- (void)viewDidLoad {
    
    _contentView.layer.cornerRadius = 2;
    [self p_setColorsForViews];
}

- (void)p_setColorsForViews {
    
    _damagePhotoLB.textColor       = [self defaultTextColor];
    _contentView.backgroundColor   = [self defaultBGColor];
    
    _makePhotoView.backgroundColor = [self defaultDarkBGColor];
    _makePhotoLB.textColor         = [self defaultTextColor];
    _makePhotoIV.image = [_makePhotoIV.image imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
    _makePhotoIV.tintColor = [UIColor whiteColor];
    
    _pickPhotoView.backgroundColor = [self defaultDarkBGColor];
    _pickPhotoLB.textColor         = [self defaultTextColor];
    _pickPhotoIV.image = [_pickPhotoIV.image imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
    _pickPhotoIV.tintColor = [UIColor whiteColor];
    
    _firstLB.textColor  = [self defaultTextColor];
    _secondLB.textColor = [self defaultTextColor];
}

- (void)viewWillAppear:(BOOL)animated {
    
    _damagePhotoLB.text = _photoType == MCPhotoTypeCar ? @"ФОТО АВТОМОБИЛЯ" : @"ФОТО ВМЯТИНЫ";
    [UIView animateWithDuration: 0.3 animations:^{
        self.view.backgroundColor = [[UIColor alloc] initWithRed: 0.0 green: 0.0 blue: 0.0 alpha: 0.6];
    }];
}

- (IBAction)choosePhotoBtnTouched:(UIButton *)sender {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    _fromCamera = NO;
    [self presentViewController:imagePickerController animated: YES completion: nil];
}

- (IBAction)cancleBtnTouched:(UIButton *)sender {
    [UIView animateWithDuration: 0.2 animations:^{
        self.view.backgroundColor = [UIColor clearColor];
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated: YES completion: ^{
            [self.presentationParent markUpPhotoIcons];
        }];
    }];
}

- (void)checkChosenPhotoCount:(UIImagePickerController *)picker {
    
    if (picker != nil) {
        [picker dismissViewControllerAnimated: YES completion: nil];
    }
    
    if (self.photoType == MCPhotoTypeDamage) {
        
        NSUInteger damagePhotoCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"damagePhotosCount"] integerValue];
        
        if (damagePhotoCount < 3 && damagePhotoCount > 0 && _fromCamera == NO) {
            
            [self p_showLastPhotoCountAlert: damagePhotoCount];
            
        } else {
            [self dismissViewControllerAnimated: YES completion: ^{
                [self.presentationParent markUpPhotoIcons];
            }];
        }
    } else if (self.photoType == MCPhotoTypeCar) {
        
        NSUInteger carPhotoCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"carPhotosCount"] integerValue];
        
        if (carPhotoCount > 0) {
            [self dismissViewControllerAnimated:YES completion:^{
                [self.presentationParent markUpPhotoIcons];
            }];
        }
    }
}

- (void)p_showLastPhotoCountAlert: (NSUInteger)photoCount {
    NSUInteger numberPhotoToAdd = 3 - photoCount;
    NSString *photoWord = numberPhotoToAdd == 1 ? @"фотографию" : @"фотографии";
    NSString *alertTitle = [NSString stringWithFormat: @"Вы можете добавить еще %li %@.", (long)numberPhotoToAdd, photoWord];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: alertTitle
                                                    message: @""
                                                   delegate: self
                                          cancelButtonTitle: @"Готово"
                                          otherButtonTitles: @"Добавить", nil];
    alert.tag = 1;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        if (buttonIndex == 1) {
            [self choosePhotoBtnTouched:nil];
        } else if (buttonIndex == 0) {
            [self dismissViewControllerAnimated:YES completion: nil];
        }
    }
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenPhoto = [UIImage imageWithImage: info[UIImagePickerControllerOriginalImage]
                                     scaledToWidth: 1080];
    NSData *photoData = UIImageJPEGRepresentation(chosenPhoto, 0.5);
    
    [[MCPhotoManager shr] saveOnePhoto:photoData photoType: self.photoType];
    
    [self checkChosenPhotoCount:picker];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self checkChosenPhotoCount:picker];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue destinationViewController] isKindOfClass:[MCPhotoMaker class]]) {
        MCPhotoMaker *photoMaker = (MCPhotoMaker *)[segue destinationViewController];
        photoMaker.delegate = self.presentationParent;
        photoMaker.presentationParent = self;
        photoMaker.photoType = self.photoType;
    }
}

@end



