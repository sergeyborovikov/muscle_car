//
//  UIImage+Resize.h
//  MuscleCar
//
//  Created by Sergey Borovikov on 27/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Resize)

+ (UIImage *)imageWithImage:(UIImage *)image
               scaledToSize:(CGSize)newSize;

//method to scale image accordcing to width
+ (UIImage *)imageWithImage:(UIImage*)sourceImage
              scaledToWidth:(float)i_width;

@end
