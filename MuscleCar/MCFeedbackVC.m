//
//  FeedbackVC.m
//  MuscleCar
//
//  Created by Sergey Borovikov on 26/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//
#import <MailCore/MailCore.h>
#import "MCFeedbackVC.h"
#import "MCPhotoManager.h"
#import "UIViewController+Colors.h"
#import "UIViewController+PhoneFormat.h"
#import "UIViewController+InputAccessoryView.h"
#import "UIView+Shadow.h"
#import "MCInfoVC.h"

@interface MCFeedbackVC ()
<UITextFieldDelegate,
MCInputAccesViewDelegate,
UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *titleLB;

// Nav Bar View
@property (weak, nonatomic) IBOutlet UIView      *navBarView;
@property (weak, nonatomic) IBOutlet UIImageView *muscleCarLogoIV;
@property (weak, nonatomic) IBOutlet UIView      *backBtnView;
@property (weak, nonatomic) IBOutlet UIImageView *arrowLeftIV;

@property (weak, nonatomic) IBOutlet UILabel *labelLB;
@property (weak, nonatomic) IBOutlet UIView  *lineView;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UILabel *plusSevenLB;

// Send Btn View
@property (weak, nonatomic) IBOutlet UIView  *sendBtnView;
@property (weak, nonatomic) IBOutlet UILabel *sendBtnLB;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicater;

@end

@implementation MCFeedbackVC {
    MCPhotoManager *photoManager;
}

- (void)viewDidLoad {
    _phoneTF.inputAccessoryView = [self inputAccesView];
    photoManager = [MCPhotoManager shr];
    [self p_setPhoneNumberTF];
    [self p_setColorForViews];
    [self p_setFontAndColorForLabels];
}

- (void)viewDidAppear:(BOOL)animated {
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [self p_savePhoneNumber];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [self p_savePhoneNumber];
}

- (void)p_setColorForViews {
    
    self.view.backgroundColor    = [self defaultBGColor];
    _navBarView.backgroundColor  = [self defaultBGColor];
    _lineView.backgroundColor    = [self defaultRedColor];
    _sendBtnView.backgroundColor = [self defaultBtnColor];
    _phoneTF.textColor           = [self defaultTextColor];
    _sendBtnView.layer.cornerRadius = 2;
    [_sendBtnView addShadow];
    _sendBtnLB.textColor = [self defaultBGColor];
}

- (void)p_setFontAndColorForLabels {
    
    _titleLB.textColor = [self defaultTextColor];
    _titleLB.font = [UIFont fontWithName: @"RoadRadio-Light" size: 20.0];
    _labelLB.textColor     = [self defaultTextColor];
    _plusSevenLB.textColor = [self defaultTextColor];
    _sendBtnLB.textColor   = [self defaultBGColor];
    // set custom color for placeholder
    [_phoneTF setValue:[self defaultTextColor] forKeyPath:@"_placeholderLabel.textColor"];
}

- (void)p_savePhoneNumber {
    [[NSUserDefaults standardUserDefaults] setObject: _phoneTF.text forKey:@"myPhone"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)p_setPhoneNumberTF {
    NSString *phoneNumberStr = [[NSUserDefaults standardUserDefaults] objectForKey: @"myPhone"];
    _phoneTF.text = phoneNumberStr == nil ? @"" : phoneNumberStr;
}

- (IBAction)backBtnTouched:(id)sender {
    [self.navigationController popToViewController: self.navigationController.viewControllers[1]
                                          animated: YES];
}

- (IBAction)sendBtnTouched:(id)sender {
    
    if(_activityIndicater.isAnimating) { return; }
    
    if (_phoneTF.text.length < 14 ) {
        [self p_showWrongPhoneNumber];
        return;
    }
    
    [_activityIndicater startAnimating];
    
    MCOSMTPSession *smtpSession = [[MCOSMTPSession alloc] init];
    smtpSession.hostname = @"smtp.mail.ru";
    smtpSession.port     = 465;
    smtpSession.username = @"musclecar-mail@mail.ru";
    smtpSession.password = @"s+WZkH*Sna3sXL";
    smtpSession.authType = MCOAuthTypeSASLNone;
    smtpSession.connectionType = MCOConnectionTypeTLS;
    
    MCOMessageBuilder *builder = [[MCOMessageBuilder alloc] init];
    MCOAddress *from = [MCOAddress addressWithDisplayName:@"musclecar-mail@mail.ru" mailbox:@"musclecar-mail@mail.ru"];
    MCOAddress *to      = [MCOAddress addressWithDisplayName: nil mailbox: @"pdr@musclecar66.ru"];
    MCOAddress *toTest1 = [MCOAddress addressWithDisplayName: nil mailbox: @"sergey.borovikov@list.ru"];
    MCOAddress *toTest2 = [MCOAddress addressWithDisplayName: nil mailbox: @"ilya.nikulin@gmail.com"];
    
    NSDateFormatter *dateFormator = [[NSDateFormatter alloc] init];
    [dateFormator setDateFormat:@"dd MMM YYYY  hh:mm"];
    [dateFormator setTimeZone: [NSTimeZone localTimeZone]];
    NSString *date = [dateFormator stringFromDate: [NSDate date]];
    NSString *subject = [NSString stringWithFormat: @"iOS Muscle Car тел: +7 %@", _phoneTF.text];
    NSString *damageDitail = [[NSUserDefaults standardUserDefaults] objectForKey:@"damageDitail"];
    NSString *body = [NSString stringWithFormat: @"Дата: %@ \n Контакт: +7 %@ \n Название детали: %@\n\n\n",
                                 date,
                                 _phoneTF.text,
                                 damageDitail];
    
    [[builder header] setFrom: from];
    [[builder header] setTo: @[to, toTest1, toTest2]];
    [[builder header] setSubject: subject];
    
    NSArray *allPhotos = [photoManager getAllPhotosFromDocuments];
    
    for (NSData *photoData in allPhotos) {
        MCOAttachment *attachment = [MCOAttachment attachmentWithData:photoData filename:@"Muscle_Car_photo.jpeg"];
        [builder addAttachment: attachment];
    }
    
    [builder setTextBody: body];
    
    NSData *msgData = [builder data];
    
    MCOSMTPSendOperation *sendOpertion = [smtpSession sendOperationWithData: msgData];
    [sendOpertion start: ^(NSError *error) {
        if(error) {
            [self p_showFaildSendEmailAlert];
        } else {
            [self performSegueWithIdentifier: @"toThanksVC" sender:self];
            [photoManager removeAllPhotos];
            [[NSUserDefaults standardUserDefaults] setObject: @"" forKey: @"damageDitail"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        [_activityIndicater stopAnimating];
    }];
    [self p_savePhoneNumber];
}


- (void)p_showFaildSendEmailAlert {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Невозможно отправить данные"
                                                    message: @"Проверьте есть ли подключение к интернет.\n Вы можете отправить данные прямо сейчас или позднее."
                                                   delegate: self
                                          cancelButtonTitle: @"Закрыть"
                                          otherButtonTitles: @"Отправить", nil];
    alert.tag = 2;
    [alert show];
}

- (void)p_showWrongPhoneNumber {
    [[[UIAlertView alloc] initWithTitle: @"Некоректный номер телефона"
                                message: nil
                               delegate: nil
                      cancelButtonTitle: @"Продолжить"
                      otherButtonTitles: nil] show];
    return;
}

# pragma mark UIAlerViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        [_phoneTF becomeFirstResponder];
    } else if (alertView.tag == 2 && buttonIndex == 1) {
        [self sendBtnTouched:nil];
    }
}

# pragma mark UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGFloat newOffsetY = textField.frame.origin.y - 100;
    [_scrollView setContentOffset:CGPointMake(0.0, newOffsetY) animated:YES];
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [_scrollView setContentOffset: CGPointMake(0,0) animated: YES];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self.view endEditing:YES];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    
    if (range.length == 1) {
        // Delete button was hit.. 
        textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
    } else {
        textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
    }
    return NO;
}

# pragma mark InputAccesViewDelegate
- (void)okTouched     { [self.view endEditing: YES]; }
- (void)cancelTouched { [self.view endEditing: YES]; }

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue destinationViewController] isKindOfClass:[MCInfoVC class]]) {
        MCInfoVC *infoVC = (MCInfoVC *)[segue destinationViewController];
        infoVC.presentedVCIndex = @2;
    }
}

@end














