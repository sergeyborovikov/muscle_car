//
//  UIViewController+PhoneFormat.h
//  MuscleCar
//
//  Created by Sergey Borovikov on 27/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (PhoneFormat)

- (NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar;

@end
