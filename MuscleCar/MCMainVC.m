//
//  MainVC.m
//  MuscleCar
//
//  Created by Sergey Borovikov on 23/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import "MCInfoVC.h"
#import "MCMainVC.h"
#import "MCPhotoVC.h"
#import "MCPhotoMaker.h"
#import "MCPhotoManager.h"
#import "UIViewController+Colors.h"
#import "UIView+Shadow.h"

@interface MCMainVC () <UITextFieldDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

// Nav Bar View
@property (weak, nonatomic) IBOutlet UIView      *navBarView;
@property (weak, nonatomic) IBOutlet UIImageView *muscleCarLogoIV;
@property (weak, nonatomic) IBOutlet UIView      *infoBtnView;
@property (weak, nonatomic) IBOutlet UIImageView *infoIV;

@property (weak, nonatomic) IBOutlet UILabel *descriptionLB;

// Photo BTNs
@property (weak, nonatomic) IBOutlet UIView  *damageCarView;
@property (weak, nonatomic) IBOutlet UIButton *damagePhotoBtn;
@property (weak, nonatomic) IBOutlet UILabel *damagePhotoLB;
@property (weak, nonatomic) IBOutlet UILabel *damagePhoto2LB;
@property (weak, nonatomic) IBOutlet UIImageView *damagePhotoIconIV;

@property (weak, nonatomic) IBOutlet UIView  *carPhotoView;
@property (weak, nonatomic) IBOutlet UIButton *carPhotoBtn;
@property (weak, nonatomic) IBOutlet UILabel *carPhotoLB;
@property (weak, nonatomic) IBOutlet UIImageView *carPhotoIconIV;

// damage Ditailes Text Field
@property (weak, nonatomic) IBOutlet UILabel *textFieldDescriptionLB;
@property (weak, nonatomic) IBOutlet UILabel *forExampleLB;
@property (weak, nonatomic) IBOutlet UILabel *ditailLB;
@property (weak, nonatomic) IBOutlet UITextField *damageDitailTF;
@property (weak, nonatomic) IBOutlet UIView  *lineView;

// Begin Btn View
@property (weak, nonatomic) IBOutlet UIView  *forwordBtnView;
@property (weak, nonatomic) IBOutlet UILabel *forwordBtnLB;

@end

@implementation MCMainVC {
    MCPhotoType tmpChosenPhototype;
    MCPhotoManager *_photoManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _photoManager = [MCPhotoManager shr];
    [self p_setFontAndColorForLables];
    [self p_setColorForViews];
}

- (void)viewWillAppear:(BOOL)animated {
    
    _forwordBtnView.layer.cornerRadius = 2;
    [_forwordBtnView addShadow];
    [self p_setDamageDetailTF];
    [self markUpPhotoIcons];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [self p_saveDamageDitail];
}

- (void)p_setColorForViews {
    
    self.view.backgroundColor       = [self defaultBGColor];
    _navBarView.backgroundColor     = [self defaultBGColor];
    _forwordBtnView.backgroundColor = [self defaultBtnColor];
    _forwordBtnLB.textColor         = [self defaultBGColor];
    
    _damageCarView.backgroundColor = [self defaultDarkBGColor];
    _carPhotoView.backgroundColor  = [self defaultDarkBGColor];
    _lineView.backgroundColor      = [self defaultRedColor];
}

- (void)p_setFontAndColorForLables {
    
    _descriptionLB.font  = [UIFont fontWithName: @"RoadRadio-Light" size: 20.0];
    _damagePhotoLB.font  = [UIFont fontWithName: @"RoadRadio-Light" size: 20.0];
    _damagePhoto2LB.font = [UIFont fontWithName: @"RoadRadio-Thin"  size: 16.0];
    _carPhotoLB.font     = [UIFont fontWithName: @"RoadRadio-Light" size: 20.0];
    
    _descriptionLB.textColor  = [self defaultTextColor];
    _damagePhotoLB.textColor  = [self defaultTextColor];
    _damagePhoto2LB.textColor = [self defaultTextColor];
    _carPhotoLB.textColor     = [self defaultTextColor];

    _textFieldDescriptionLB.textColor = [self defaultTextColor];
    _forExampleLB.textColor   = [self defaultTextColor];
    _ditailLB.textColor       = [self defaultTextColor];
    _damageDitailTF.textColor = [self defaultTextColor];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [self p_saveDamageDitail];
}

- (IBAction)damageOrCarPhotoBtnTouched:(UIButton *)sender {
    if (sender.tag == 0) {
        if ([_photoManager areThereDamagePhotoInDocuments]) {
            tmpChosenPhototype = MCPhotoTypeDamage;
            [self p_showNewPhotoAlert];
        } else {
            [self performSegueWithIdentifier:@"toPhotoVC" sender:_damagePhotoBtn];
        }
    } else if (sender.tag == 1) {
        if ([_photoManager areThereCarPhotoInDocuments]) {
            tmpChosenPhototype = MCPhotoTypeCar;
            [self p_showNewPhotoAlert];
        } else {
            [self performSegueWithIdentifier: @"toPhotoVC" sender:_carPhotoBtn];
        }
    }
}

- (IBAction)forwordBntTouched:(UIButton *)sender {
    
    // Если есть фото автомобиля и нет ни фото повреждений ни информации о детали
    if (([_photoManager areThereCarPhotoInDocuments] &&
        ![_photoManager areThereDamagePhotoInDocuments]) &&
         ([_damageDitailTF.text isEqualToString:@""] ||
         _damageDitailTF.text == nil)) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Предоставлена не вся информация"
                                                        message: @"Отсутствует фото повреждения или название детали."
                                                       delegate: self
                                              cancelButtonTitle: @"Вернуться"
                                              otherButtonTitles: @"Продолжить", nil];
        alert.tag = 1; // for recognition..
        [alert show];
        
    } else if (![_photoManager areThereDamagePhotoInDocuments] &&
               ![_photoManager areThereCarPhotoInDocuments] ) {
            
        [[[UIAlertView alloc] initWithTitle: @"Отсутствуют фотографии"
                                    message: @"Необходимо сделать хотя бы одно фото."
                                   delegate: nil
                          cancelButtonTitle: nil
                          otherButtonTitles: @"Закрыть", nil] show];
                              
    } else {
        [self performSegueWithIdentifier:@"toFeedbackVC" sender:self];
    }
}

- (IBAction)backBtnTouched:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)p_saveDamageDitail {
    [[NSUserDefaults standardUserDefaults] setObject: _damageDitailTF.text forKey:@"damageDitail"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)p_setDamageDetailTF {
    NSString *damageDitailStr = [[NSUserDefaults standardUserDefaults] objectForKey: @"damageDitail"];
    _damageDitailTF.text = damageDitailStr == nil ? @"" : damageDitailStr;
}

- (void)markUpPhotoIcons {
    
    if ([_photoManager areThereDamagePhotoInDocuments]) {
        _damagePhotoIconIV.image = [UIImage imageNamed: @"doneIcon"];
    } else {
        _damagePhotoIconIV.image = [UIImage imageNamed: @"photoIcon"];
    }
    
    if ([_photoManager areThereCarPhotoInDocuments]) {
        _carPhotoIconIV.image = [UIImage imageNamed: @"doneIcon"];
    } else {
        _carPhotoIconIV.image = [UIImage imageNamed: @"photoIcon"];
    }
}

- (void)p_showNewPhotoAlert {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Вы уже сохранили фотографии в данном разделе."
                                                    message: @"Начать с начала?"
                                                   delegate: self
                                          cancelButtonTitle: @"Отменить"
                                          otherButtonTitles: @"Сначала", nil];
    alert.tag = 3; // for recognition..
    [alert show];
}

#pragma mark UIAlerViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    UIButton *toppedBtn = tmpChosenPhototype == MCPhotoTypeDamage ? _damagePhotoBtn : _carPhotoBtn;
    
    if (alertView.tag == 1 && buttonIndex == 1) {
        [self performSegueWithIdentifier:@"toFeedbackVC" sender: toppedBtn ];
    } else if (alertView.tag == 2 && buttonIndex == 1) {
        [self performSegueWithIdentifier:@"toFeedbackVC" sender: toppedBtn ];
    } else if (alertView.tag == 3 && buttonIndex == 1) {
        if (tmpChosenPhototype == MCPhotoTypeDamage) {
            [_photoManager removeDamagePhotos];
        } else if (tmpChosenPhototype == MCPhotoTypeCar) {
            [_photoManager removeCarPhoto];
        }
        [self performSegueWithIdentifier:@"toPhotoVC" sender: toppedBtn ];
    }
}

#pragma mark PhotoMakerDelegate
- (void)savePhotos:(NSArray *)photos photoType:(MCPhotoType)photoType {
    [_photoManager savePhotos: photos photoType: photoType];
    [self markUpPhotoIcons];
}

# pragma mark UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGFloat newOffsetY = textField.frame.origin.y - 100;
    [_scrollView setContentOffset:CGPointMake(0.0, newOffsetY) animated:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [_scrollView setContentOffset: CGPointMake(0,0) animated: YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton *)sender {
    
    if ([[segue destinationViewController] isKindOfClass:[MCPhotoVC class]]) {
        MCPhotoVC *photoVC = (MCPhotoVC *)[segue destinationViewController];
        photoVC.presentationParent = self;
        photoVC.view.backgroundColor = [UIColor clearColor];
        photoVC.photoType = sender.tag == 0 ? MCPhotoTypeDamage : MCPhotoTypeCar;
    }
    
    if ([[segue destinationViewController] isKindOfClass:[MCInfoVC class]]) {
        MCInfoVC *infoVC = (MCInfoVC *)[segue destinationViewController];
        infoVC.presentedVCIndex = @1;
    }
}

@end












