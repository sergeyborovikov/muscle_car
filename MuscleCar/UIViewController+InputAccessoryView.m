//
//  UIViewController+InputAccessoryView.m
//  MuscleCar
//
//  Created by Sergey Borovikov on 24/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import "UIViewController+InputAccessoryView.h"
#import "UIViewController+Colors.h"

@implementation UIViewController (InputAccessoryView)

- (UIView *)inputAccesView {
    
    CGSize mSize = [[UIScreen mainScreen] bounds].size;
    CGRect inputAccesRect = CGRectMake(0,
                                       0,
                                       mSize.width,
                                       58);
    UIView *inputAccesView = [[UIView alloc] initWithFrame: inputAccesRect];
    inputAccesView.backgroundColor = [UIColor whiteColor];
    
    // Ok Btn
    UIButton *okBtn = [[UIButton alloc] initWithFrame:CGRectMake(mSize.width - 100,
                                                                 0,
                                                                 100,
                                                                 inputAccesView.frame.size.height)];
    [okBtn setTitle: @"OK" forState: UIControlStateNormal];
    [okBtn setTitleColor: [self defaultBGColor] forState: UIControlStateNormal];
    [okBtn addTarget: self action: @selector(okTouched) forControlEvents: UIControlEventTouchUpInside];
    [inputAccesView addSubview: okBtn];
    
    // Cancel Btn
//    UIButton *cancelBtn = [[UIButton alloc] initWithFrame: CGRectMake(0,
//                                                                      0,
//                                                                      120,
//                                                                      inputAccesView.frame.size.height)];
//    [cancelBtn setTitle: @"Отмена" forState: UIControlStateNormal];
//    [cancelBtn setTitleColor: [self defaultBGColor] forState: UIControlStateNormal];
//    [cancelBtn addTarget:self action:@selector(cancelTouched) forControlEvents:UIControlEventTouchUpInside];
//    [inputAccesView addSubview: cancelBtn];
    
    return inputAccesView;
}

@end
