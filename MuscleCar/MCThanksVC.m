//
//  ThanksVC.m
//  MuscleCar
//
//  Created by Sergey Borovikov on 23/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import "MCThanksVC.h"
#import "MCMainVC.h"
#import "UIViewController+Colors.h"
#import "UiView+Shadow.h"
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>

@interface MCThanksVC () <CNContactViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *thanksLB;
@property (weak, nonatomic) IBOutlet UILabel *weRecallLB;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *titleLB;

// Nav Bar View
@property (weak, nonatomic) IBOutlet UIView      *navBarView;
@property (weak, nonatomic) IBOutlet UIImageView *muscleCarLogoIV;
@property (weak, nonatomic) IBOutlet UIView      *backBtnView;
@property (weak, nonatomic) IBOutlet UIImageView *arrowLeftIV;

// BTN views
@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UIView *phoneView;
@property (weak, nonatomic) IBOutlet UIView *webSideView;
@property (weak, nonatomic) IBOutlet UIView *saveContactView;

@property (weak, nonatomic) IBOutlet UILabel *addressLB;
@property (weak, nonatomic) IBOutlet UILabel *phoneLB;
@property (weak, nonatomic) IBOutlet UILabel *webSiteLB;
@property (weak, nonatomic) IBOutlet UILabel *saveToContactsLB;

@property (weak, nonatomic) IBOutlet UIView  *beginBtnView;
@property (weak, nonatomic) IBOutlet UILabel *beginBtnLB;

@end

@implementation MCThanksVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [self defaultBGColor];
    [self p_setColorForLables];
    [self p_setColorForViews];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    _beginBtnView.layer.cornerRadius = 2;
    [_beginBtnView addShadow];
}

- (void)p_setColorForViews {
    
    self.view.backgroundColor    = [self defaultBGColor];
    _titleLB.textColor           = [self defaultRedColor];
    _navBarView.backgroundColor  = [self defaultBGColor];
    _addressView.backgroundColor = [self defaultDarkBGColor];
    _phoneView.backgroundColor   = [self defaultDarkBGColor];
    _webSideView.backgroundColor = [self defaultDarkBGColor];
    _saveContactView.backgroundColor = [self defaultDarkBGColor];
}

- (void)p_setColorForLables {
    
    _thanksLB.textColor         = [self defaultTextColor];
    _weRecallLB.textColor       = [self defaultTextColor];
    _addressLB.textColor        = [self defaultTextColor];
    _phoneLB.textColor          = [self defaultTextColor];
    _webSiteLB.textColor        = [self defaultTextColor];
    _saveToContactsLB.textColor = [self defaultTextColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)beginBtnTouched:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)backBntTouched:(UIButton *)sender {
    MCMainVC *mainVC = (MCMainVC *)self.navigationController.viewControllers[1];
    [self.navigationController popToViewController:mainVC animated:YES];
}

- (IBAction)openMuscleCarSite:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.musclecar66.ru"]];
}

- (IBAction)call:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://83433288861"]];
}

- (IBAction)saveContactBtnTouched:(UIButton *)sender {
    
    CNContactStore *store = [[CNContactStore alloc] init];
    
    // create contact
    CNMutableContact *contact = [[CNMutableContact alloc] init];
    contact.familyName = @"Автоцентр";
    contact.givenName  = @"Мускулкар";
    
    CNLabeledValue *homePhone = [CNLabeledValue labeledValueWithLabel:CNLabelHome value:[CNPhoneNumber phoneNumberWithStringValue:@"+7 (343) 328-88-61"]];
    CNLabeledValue *email = [CNLabeledValue labeledValueWithLabel:CNLabelEmailiCloud value:@"pdr@musclecar66.ru"];
    CNLabeledValue *url = [CNLabeledValue labeledValueWithLabel:CNLabelURLAddressHomePage value:@"http://www.musclecar66.ru"];
    contact.phoneNumbers   = @[homePhone];
    contact.emailAddresses = @[email];
    contact.urlAddresses   = @[url];
    
    CNContactViewController *contactController = [CNContactViewController viewControllerForUnknownContact:contact];
    contactController.contactStore = store;
    contactController.delegate = self;
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController pushViewController:contactController animated: YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end


