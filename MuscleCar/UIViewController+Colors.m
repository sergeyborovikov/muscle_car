//
//  UIViewController+Colors.m
//  MuscleCar
//
//  Created by Sergey Borovikov on 23/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import "UIViewController+Colors.h"

@implementation UIViewController (Colors)

-(UIColor*)defaultBGColor     { return UIColorFromRGB(0x00395a); }
-(UIColor*)defaultDarkBGColor { return UIColorFromRGB(0x00324f); }
-(UIColor*)defaultTextColor   { return UIColorFromRGB(0xaebcba); }
-(UIColor*)defaultBtnColor    { return UIColorFromRGB(0xaebcba); }
-(UIColor*)defaultRedColor    { return UIColorFromRGB(0xd9531e); }

-(UIColor*)defaultBGColorWithAlpha {
    return UIColorFromRGBalpha(0x00395a);
}

@end
