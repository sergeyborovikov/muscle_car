//
//  MCMapVC.m
//  MuscleCar
//
//  Created by Sergey Borovikov on 28/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import "MCMapVC.h"
#import <MapKit/MapKit.h>
#import "UIViewController+Colors.h"

@interface MCMapVC()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

// Nav Bar
@property (weak, nonatomic) IBOutlet UIView *navBarView;
@property (weak, nonatomic) IBOutlet UIView *backBtnView;

@end

@implementation MCMapVC

- (void)viewDidLoad {
    _navBarView.backgroundColor = [self defaultBGColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.view bringSubviewToFront:_navBarView];
}

- (void)viewDidAppear:(BOOL)animated {
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(56.894243990871665, 60.618460178375244);
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(location, 500, 500);
    [_mapView setRegion:viewRegion animated:YES];
    
    // Place a single pin
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:location];
    [annotation setTitle:@"Автоцентр Мускулкар"]; //You can set the subtitle too
    [self.mapView addAnnotation:annotation];
}
- (IBAction)backBtnTouched:(id)sender {
    [self dismissViewControllerAnimated:YES completion: nil];
}

@end
