//
//  PhotoManager.m
//  MuscleCar
//
//  Created by Sergey Borovikov on 27/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import "MCPhotoManager.h"

@implementation MCPhotoManager {
    NSUserDefaults *_userDefaults;
    NSFileManager  *_fileManager;
    NSString *_documentsDirectory;
}

+ (instancetype)shr {
    static MCPhotoManager *singltone;
    static dispatch_once_t t;
    dispatch_once(&t, ^{
        if (singltone == nil) {
            singltone = [[self alloc] init];
        }
    });
    return singltone;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _userDefaults = [NSUserDefaults standardUserDefaults];
        _fileManager  = [NSFileManager defaultManager];
        _documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                   NSUserDomainMask,
                                                                   YES) objectAtIndex:0];
    }
    return self;
}

- (void)savePhotos:(NSArray *)photos photoType:(MCPhotoType)photoType {
    
    // save photos to Documents
    if (photoType == MCPhotoTypeDamage) {
        
        NSUInteger damagePhotoCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"damagePhotosCount"] integerValue];
        for (NSUInteger i = 0; i < photos.count; i++) {
            NSData *photoData = photos[i];
            NSString *photoPath = [_documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/dp%li.png",(long)i + damagePhotoCount]];
            [photoData writeToFile:photoPath atomically:YES];
        }
        [_userDefaults setObject:@(photos.count) forKey:@"damagePhotosCount"];
        [_userDefaults synchronize];
    } else if (photoType == MCPhotoTypeCar) {
        [self saveOnePhoto:photos[0] photoType:MCPhotoTypeCar];
        [_userDefaults setObject:[NSNumber numberWithInteger:photos.count] forKey:@"carPhotosCount"];
        [_userDefaults synchronize];
    }
    NSLog(@"saveDamagePhotos(delegate): %li", (long)photos.count);
}

- (void)saveOnePhoto:(NSData *)photoData photoType:(MCPhotoType)photoType {
    if (photoType == MCPhotoTypeDamage) {
        [self p_saveOneDamagePhoto:photoData];
    } else if (photoType == MCPhotoTypeCar) {
        [self p_saveCarPhoto:photoData];
    }
}

- (NSArray *)getAllPhotosFromDocuments {
    NSMutableArray *result = [NSMutableArray array];
    
    [result addObjectsFromArray:[self getDamagePhotosFromDocuments]];
    [result addObjectsFromArray:[self getCarPhotosFromDocuments]];
    
    return result;
}

- (NSArray *)getDamagePhotosFromDocuments {
    NSMutableArray *result = [NSMutableArray array];
    NSUInteger damagePhotoCount = [[_userDefaults objectForKey:@"damagePhotosCount"] integerValue];
    for (NSUInteger i = 0; i < damagePhotoCount; i++) {
        NSString *pathToDamagePhoto = [_documentsDirectory stringByAppendingString:[NSString stringWithFormat:@"/dp%li.png", i]];
        NSData *damagePhotoData = [NSData dataWithContentsOfFile: pathToDamagePhoto];
        [result addObject: damagePhotoData];
    }
    return result;
}
- (NSArray *)getCarPhotosFromDocuments {
    NSMutableArray *result = [NSMutableArray array];
    NSUInteger carPhotoCount = [[_userDefaults objectForKey:@"carPhotosCount"] integerValue];
    if (carPhotoCount > 0) {
        NSString *pathToCarPhoto = [_documentsDirectory stringByAppendingString:[NSString stringWithFormat:@"/car.png"]];
        NSData *carPhotoData = [NSData dataWithContentsOfFile: pathToCarPhoto];
        [result addObject: carPhotoData];
    }
    return result;
}

- (BOOL)areThereDamagePhotoInDocuments {
    NSUInteger damagePhotoCount = [[_userDefaults objectForKey:@"damagePhotosCount"] integerValue];
    return (damagePhotoCount > 0);
}

- (BOOL)areThereCarPhotoInDocuments {
    NSUInteger carPhotoCount = [[_userDefaults objectForKey:@"carPhotosCount"] integerValue];
    return (carPhotoCount > 0);
}

- (void)removeAllPhotos {
    [self removeDamagePhotos];
    [self removeCarPhoto];
}

- (void)removeDamagePhotos {
    NSUInteger damagePhotoCount = [[_userDefaults objectForKey:@"damagePhotosCount"] integerValue];
    for(NSUInteger i = 0; i < damagePhotoCount; i++) {
        NSString *pathPhoto = [_documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"dp%li.png", (long)i]];
        if(pathPhoto != nil) { [self p_removePhotoFromDocuments: pathPhoto]; }
    }
    [_userDefaults setObject:@0 forKey:@"damagePhotosCount"];
}

- (void)removeCarPhoto {
    NSUInteger carPhotosCount   = [[_userDefaults objectForKey:@"carPhotosCount"] integerValue];
    if (carPhotosCount > 0) {
        NSString *pathPhoto = [_documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/car.png"]];
        if(pathPhoto != nil) { [self p_removePhotoFromDocuments: pathPhoto]; }
    }
    [_userDefaults setObject:@0 forKey:@"carPhotosCount"];
}


- (void)p_saveOneDamagePhoto:(NSData *)photoData {
    NSArray *damagePhotoArray = [self getDamagePhotosFromDocuments];
    NSMutableArray *mDamagePhotoArray = [damagePhotoArray mutableCopy];
    
    [self removeDamagePhotos];
    
    NSUInteger damagePhotoArrayCount = [mDamagePhotoArray count];
    while (damagePhotoArrayCount > 2) {
        [mDamagePhotoArray removeObjectAtIndex:0];
        damagePhotoArrayCount = [mDamagePhotoArray count];
    }
    
    [mDamagePhotoArray addObject:photoData];
    [self savePhotos:mDamagePhotoArray photoType:MCPhotoTypeDamage];
}

- (void)p_saveCarPhoto:(NSData *)photoData {
    [self removeCarPhoto];
    NSString *photoPath = [_documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/car.png"]];
    [photoData writeToFile:photoPath atomically:YES];
    [_userDefaults setObject:@1 forKey:@"carPhotosCount"];
    [_userDefaults synchronize];
}

- (void)p_removePhotoFromDocuments:(NSString *)fullPath {
    
    NSError *error = nil;
    if(![_fileManager removeItemAtPath: fullPath error:&error]) {
        NSLog(@"Delete failed:%@", error);
    } else {
        NSLog(@"image removed: %@", fullPath);
    }
}

@end


