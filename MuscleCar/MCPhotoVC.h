//
//  PhotoVC.h
//  MuscleCar
//
//  Created by Sergey Borovikov on 25/11/15.
//  Copyright © 2015 Sergey Borovikov. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MCMainVC;

@interface MCPhotoVC : UIViewController

@property (weak, nonatomic) NSMutableArray *photos;

@property (strong) MCMainVC *presentationParent;
@property MCPhotoType photoType;
@property BOOL fromCamera;

- (void)checkChosenPhotoCount:(UIImagePickerController *)picker;

@end
